\chapterspecial{Ensaio I}{}{}
 \begin{flushright}\emph{Ao Odilon Moraes}\end{flushright}
 \epigraph{O momento decisivo no desenvolvimento é um todo contínuo. É~por isso que
estão certos os movimentos revolucionários, que declaram nulo ou inútil
tudo o que ocorreu antes deles, pois nada aconteceu ainda.
\versal{FRANZ} \versal{KAFKA}}{\versal{FRANZ} \versal{KAFKA}} \epigraph{Fugir para uma região conquistada e logo descobrir que é intolerável,
porque não se pode fugir para nenhuma outra parte.

\versal{FRANZ} \versal{KAFKA}}{\versal{FRANZ} \versal{KAFKA}} 

\section{A \versal{ARTE} \versal{COMO} \versal{TERRITÓRIO} \versal{DO} NÃO-\versal{PODER}\footnoteInSection{Este texto foi lido no \versal{IV} Colóquio Heidegger (São Paulo), em 29 de
novembro de 1998.} }

\emph{Preâmbulo}

Minha esperança, neste texto, ao estabelecer um diálogo entre Heidegger
e Kafka, é realizar uma iluminação de mão"-dupla, isto é, fazer com que o
texto kafkiano ilumine um ``ponto cego'' de Ser e Tempo ou, para falar
de um modo menos radical, uma insuficiência no quadro de Ser e Tempo e,
num segundo momento, numa iluminação inversa, mostrar como o
``abandono'' do regime de Ser e Tempo permite, finalmente, dar conta do
que se passa no texto de Kafka. Dito brevemente, tentarei mostrar como a
passagem para o poético (Benedito Nunes) impõe a quebra do círculo
hermenêutico (Ernildo Stein) de tal modo que a agudização da finitude
alcance também a palavra do homem, convertendo a palavra"-cheia (teoria
da linguagem em Ser e Tempo) em palavra"-quebrada (palavra do poeta).

Segue o texto de Kafka:

\emph{Passageiro}\footnote{Este texto (Kafka, 1991) é uma tradução do professor Modesto Carone,
estudioso da obra de Kafka, principalmente a partir da filosofia de
Adorno. Modesto Carone traduziu também um livro de Günther Anders sobre
Kafka (Kafka Pró e Contra). Anders foi um heideggeriano de primeira hora
e no seu livro, entre outras coisas, pensa o tema kafkiano da chegada ao
mundo e da errância a partir de Ser e Tempo. A~questão do Fora aparece
também nesse belo texto heideggermarxist sem ter sido, entretanto,
desenvolvida em toda sua amplitude.} 

\begin{center}\emph{Estou em pé na plataforma do bonde elétrico e totalmente inseguro
em relação à minha posição neste mundo, nesta cidade, na minha família.
Nem de passagem eu seria capaz de apontar as reivindicações que poderia
fazer, com direito, na direção que fosse. Não posso de modo algum
sustentar que estou nesta plataforma, que me seguro nesta alça, que me
deixo transportar por este bonde, que as pessoas se desviam dele ou
andam calmamente ou param diante das vitrines. E~claro que ninguém exige
isso de mim, mas dá no mesmo.}\end{center}
 \begin{center}\emph{O~bonde se aproxima de uma parada, uma jovem se coloca perto dos
degraus pronta para descer. Aparece tão nítida para mim que é como se eu
a tivesse apalpado. Está vestida de preto, as pregas da saia quase não
se movem, a blusa é justa e tem uma gola de renda branca fina, ela
mantém a mão esquerda espalmada na parede do bonde e a sombrinha da mão
direita se apoia no penúltimo degrau mais alto. Seu rosto é moreno, o
nariz levemente amassado dos lados termina redondo e largo. Ela tem
cabelos castanhos fartos e pelinhos esvoaçando na têmpora direita. Sua
orelha pequena é bem ajustada, mas por estar próximo eu vejo toda a
parte de trás da concha direita e a sombra da base.}\end{center}
 \begin{center}\emph{Naquela ocasião eu me perguntei: como é que ela não está espantada
consigo mesma, conserva a boca fechada e não diz coisas desse tipo?}\end{center}


\subsection{I\,Heidegger e Kafka: por uma Dizibilidaâe Heterodoxa}

A ideia de pensar a arte fora dos limites da teoria estética levou
alguns homens\footnote{Penso, sobretudo, em Maurice Blanchot. Se este texto propõe um diálogo
entre Heidegger e Kafka, esse diálogo, entretanto, não se realizaria sem
a presença fundamental de Blanchot, consagrado pensador do Fora.
Acredito, porém, que a noção do Fora (ou aquela de um Espaço Literário)
pressupõe que se tenha já nomeado e compreendido o Dentro em todas as
suas figuras e possibilidades. Tal contribuição não é oriunda de
Blanchot e sim de Heidegger; sua obra inteira pode ser lida como uma
exaustiva explicitação do Dentro.}  a concebê"-la como a destituição da
verdade do mundo: destituição da presença e da permanência no tempo do
amparo no mundo. A~arte estaria, portanto, vinculada a esta lonjura, a
este ``fora'' do mundo. Ela teria a ver com aquele que --- como nos
Diários de Kafka --- zanza pelo informe território pré"-natal, ``com
aquele que se situa fora e diz e exprime (exprime entre várias aspas) a
profund.ida.de desse fora sem intimidade e sem repouso, aquilo que surge
quando, mesmo conosco, e até mesmo com a nossa morte, deixamos de ter
quaisquer relações de possibilidade. A~arte seria a consciência desse
infortúnio, o dizer de uma existência que é apenas pressentimento de
existência'' (Blanchot, 1987 e 1997), na medida em que o mundo se lhe
tornou impossível. Mas dizer que o mundo se lhe tornou impossível é
contar de uma espera sã e de uma retração que aguarda. A~situação
daquele que se perdeu, que já não pode dizer `eu', que no mesmo
movimento perdeu o mundo e a verdade do mundo, que pertence ao tempo do
exílio e do desamparo'' (Blanchot, 1987) é, paradoxalmente, a situação
daquele que pode encontrar as coisas. O~nome próprio e a história vivida
no mundo não passam de impedimentos e o costume da segurança é apenas
uma trava: perder o mundo (e eu me refiro ao mundo de Ser e Tempo) onde
se está alojado, perder a subjetividade identitária, a subjetividade
subjetiva, é poder escapar de um presídio, pois aquilo que desaloja, a
dor própria disso, é o mais hospitaleiro. Abandonar a estatura
fictícia"-sintomática do homem é encontrar sua dimensão real.

Aproximar"-se do lugar da arte --- e estou entendendo a palavra arte não
só no sentido de obras, mas como um modo de poder existir --- implica em
se fazer uma topologia, uma discriminação doDentro e do Fora, uma
clivagem entre a região da segurança no mundo (e os seus dizeres) e a
região insegura do artista. Região insegura (força do banimento) ela
mesma, para onde se é levado se convocado pela exigência da obra ou pela
medida real; radicalmente diferente, portanto, de qualquer insegurança
psicológica. A~psicologia é cega para a questão da arte, pois ela,
visando apenas administrar e disciplinarizar o caleidoscópio dos afetos
mundanos, pressupõe um sujeito do Dentro, ou seja, pressupõe aquele que
exorcizou sua relação com o Fora\footnote{Franz Kafka sabia"-o bem: ``Psicologia, pela última vez!'' (1993, p\,115)} : o psiquismo é
encobrimento (uma abstração) do existir e, falar do homem que se
relaciona com objetos é não poder falar daquele que é ``consanguíneo do
mistério das coisas'' (F\,Pessoa). No caso do pequeno texto de Kafka,
nota"-se que o narrador está totalmente inseguro, parecendo, inclusive,
não pertencer inteiramente à realidade: ``quem pertence à realidade não
teria necessidade de tantos detalhes que, como sabemos, não
correspondem, em absoluto, à forma de uma visão real'' (Blanchot, 1987).
Aquele que fala está desalojado, desapossado de si e da confiança no
mundo. A~descrição exaustiva, quase científico"-microscópica, que
determina totalmente a mulher do bonde, permanece acossada"-vigiada pelo
abismo do indeterminado. Que dizer é esse que mantém a tensão entre o
hiperdeterminado e o indeterminado? Seria essa a marca de um dizer
pós"-metafísico? Uma fala exposta ao Fora, fala que não domina o não"-ser,
mas permanece atravessada por ele? Seria essa ``insegurança ontológica''
(Giddens, 1991) a marca de uma fala inteiramente reconciliada com a
finitude? Ocuparia Kafka aquele lugar onde o ``enigma'' do acontecer da
coisa não foi arregimentado nem aniquilado pelos dizeres do Dentro? Quem
seria o homem desse dizer; qual seu afeto e tempo? Mas, antes de
investigar essa fala problemática, é preciso conhecer o verbo
pacificado. Adotando um procedimento negativo, contrapõe"-se a fala do
Fora às falas do Dentro, lá onde o sentido do que é já está garantido e
domesticado.

A primeira fala do Dentro é o conceito: o conceito se refere a uma ordem
de presenças estabelecidas. Aí se trataria de dar as características e
os atributos de uma mulher. Pouco importa se esses atributos têm um
conteúdo ontológico (logocentrismo apresentacional dos gregos) ou se
eles são meras determinações formais de qualquer objeto possível
(logocentrismo representacional dos modernos). Se no mundo antigo a
permanência do cosmo encobre o fenômeno do Dasein, no mundo moderno, a
certeza do sujeito opera o mesmo encobrimento. A~famosa passagem do
universo fechado ao mundo infinito não radicalizou o filosofar, apenas
acentuou o controle. A~ontologia da coisa, quer ela seja pensada à
maneira de Aristóteles ou no modo kantiano, está sempre associada a algo
de ``sossegado''\footnote{Põggeller (1986), em seu livro sobre Heidegger, opõe o desassossego da
vida fática ao sossego ensimesmado que vigora na ontologia da
substância. Já no curso de 1921 sobre Agostinho e o Neoplatonismo,
Heidegger prescrutáva essa oposição assinalando a diferença entre a
atitude teórica dos gregos e a experiência da vida no cristianismo
primitivo. A~partir de 1923, ele abandonará, progressivamente, o jargão
da filosofia da vida.} . Ou subsiste o mundo necessário
para o olho contemplador ou subsiste a máquina de representar que
calcula o ente. Nos dois casos, vigora a atitude teórica, vale dizer,
aquele tipo de comportamento que mais encobre, que mais esquece a
estrutura do existir humano e que de menor diálogo é capaz com a
finitude. O~chão da palavra conceituai é o Dentro absolutizado:
hipnotizada pela onipresença do ente, o dizer do conceito nomeia a
entitude de modo que nada fique fora dela (entitude). Convalescer da
entitude do ente, como o sentido único do que é, é expor"-se ao risco do
Fora, é abandonar o tempo, o afeto e o dizer do homem atual. É~mostrar
que o ``continente do Dentro não passa de uma pequena ilhota do Fora; é
operar uma revolução copernicana sem precedentes'' (Pelbart, 1989).

É possível descrever uma mulher como se descreve um ente meramente
subsistente dentro do mundo. A~situação dessa fala (logos apofântico)
faz evocar, de um lado, um olho assistente/espectador e, do outro, a
realidade sossegada composta de seus diferentes itens; é o mito
metafísico de uma realidade dada (natureza) ou criada por Deus (Idade
Média), realidades essas cujas essências (formas, \emph{eidos)} seriam
captadas pela intuição intelectual. Heidegger desconstruiu o mito
metafísico de uma realidade"-estendida"-e-pacificada e mostrou que o
comportamento teórico é apenas um comportamento (humano) possível, um
modo entre outros de se relacionar com os entes presentes. Tal
comportamento, entretanto, é o que mais repele a impermanência e a cisão
da finitude: não deixa ver o mundo como o suporte transiente onde
armamos a situação de um morar possível, não deixa ver o caráter
``quebrado e inconsistente'' do ente, bem como o admirável e o
inquietante em nosso próprio ser. A~teoria se afasta tanto da condição
mortal que Maurice Blanchot --- junto de Heidegger --- convida os
teóricos a que assumam sua mortalidade e reconheçam que a teoria já é a
morte que carregam neles (Blanchot, 1980).

No comportamento teórico, o mundo ou o sujeito estão engordados e
seguros. Nenhum deles conhece risco, pois não há ``exposição ao Fora''.
A~filosofia moderna não altera esse estado de coisas. É~um lugar"-comum
afirmar que o pensamento da modernidade começa com a dúvida e que esse
pensamento põe tudo em questão. Entretanto, conforme lembra Heidegger, a
atitude epistemologicamente crítica não é uma atitude filosoficamente
crítica. Na dúvida cartesiana, o que está em questão é apenas o saber,
``a consciência de coisas e de objetos (ou de sujeitos)'' e isso,
``somente para tornar ainda mais insistente a certeza previamente
antecipada. Mas nunca o Dasein, ele próprio, é posto em questão. Uma
atitude cartesiana fundamental em filosofia não pode, de um modo geral e
principiai, colocar em questão o Dasein do homem. Isto seria a priori
para ela, do ponto de vista de seu sentido mais próprio, um suicídio''
(Heidegger, 1992). A~atitude cartesiana condicionou o filosofar moderno;
o projeto cartesiano, que compreende o ser do ente como
representabilidade, aprofunda"-se e explicita"-se na tese kantiana do ser
do ente como condição da representabilidade e da objetividade. É~claro
que o sentido da palavra sujeito muda fortemente de Descartes para Kant,
mas essa desreificação não incide sobre o sossego e a permanência de
algo que subsiste representando. O~próprio Kant assinala que descreveu
as faculdades e o seu funcionamento plural (há vários sujeitos em Kant),
mas que sempre recuou diante da pergunta ``metatranscendental'', de como
são possíveis as faculdades mesmas, vale dizer, que a questão --- quem é
o homem? --- permaneceu apenas latente… A própria estabilidade
constanteadora do categorial cegaria Kant na apreensão de um ente
esquivo e caleidoscópico, ente estranho que, quando se deixa ver como
algo, é porque já fugiu de si e, como um camaleão, tomou a ``coloração
do ente intramundano'', enganando, assim, seu investigador.

Tanto a teoria assentada no mito da realidade como a teoria no sentido
do ``programa gnoseológico'' pertencem ao âmbito da reificação e o seu
dizer é, provavelmente, o simétrico oposto do dizer inseguro da arte. O~homem forte que aprisiona a coisa, incluindo"-a inteiramente na cela do
conceito (para o conceito não há resto), mora distante do homem frágil,
cujo dizer libera a coisa para o que lhe é próprio… É preciso
perguntar novamente: por que em Kafka a determinação da mulher do bonde
permanece vigiada pelo abismo do indeterminado? Por que todo o seu dizer
fica quebrado pela atração do inominável? Por que o excesso no dizer não
domestica a presença esmagadora do nunca dizível? O que é isso de a
coisa acenar para a doação indizível dela mesma? Uma das questões
intrigantes é a de saber por que Kant afirmou que duas coisas lhe
enchiam o ânimo de espanto e de admiração: será que é porque o minotauro
da objetivação estava apenas em sua infância? Porque Kant era ingênuo e
não imaginava que iria surgir também o Newton vienense dos astros
internos e que sua própria lei se tornaria absurda? Hoje sabemos
claramente que o programa gnoseológico da objetivação se dirigiu tanto à
natureza externa como à interna, tendo chegado ao seu apogeu
(explicitado por Nietzsche/E\,Jünger como vontade de mais poder). Se
Kant se identificou com o poder do espírito humano e achou que a vida
nos limites da razão era a vida digna, Kafka, cem anos depois, sentiu a
admiração kantiana como a opressão de uma vida vivida na cela: ``Por que
construímos presídios? Para esquecermos que já estamos em um!''. Essa
sentença de Blanchot (1980) revela a ``natureza'' do mundo percebido por
Kafka, um mundo tirânico ``cujas arregimentações e ordens se assemelham
às inexoráveis leis que estabelecem a organização da matéria
inorgânica'' (Kohut, 1973) --- mundo da instalação calculadora,
explicitado por Heidegger (Gestell).

Essa passagem por Descartes, Kant e Nietzsche é importante para
historicizar\footnote{Segundo minha tese, a história da metafísica do assim chamado
Heidegger-\versal{II} pode ser lida como história do desdobramento e da
consumação do pensamento do Dentro. O~dizer kafkiano pode emergir agora
porque todo o dizer racional esgotou"-se, transformou"-se na epopéia da
técnica.}  o dizer poético de Franz Kafka, bem
como o seu combate contra a loucura do mundo (leia"-se o sufocamento do
Dentro); sufocamento que aparece emblematicamente no destino e morte de
Gregor Samsa, bem como em Carta ao Pai (texto onde Kafka contrapõe sua
``impotência metafísica'' à potência do pai e não compreende a sua
impotência metafísica como potência poética/relação com o Fora, como
exposição ao sopro do desconhecimento). Nesse sentido, um tal dizer
pressupõe toda a história da metafísica acontecida e é inteiramente
inédito. Não se trata, portanto, de nenhuma ``revolução conservadora''
(Bourdieu, 1989) ou de um ``Heidegger"-regressivo"-nostálgico'' (Escola de
Frankfurt). Dizer o objeto a partir do Ereignis ou encontrar uma ética
de salvação das coisas pelo poema equivale a dizer, o que é uma posição
bastante paradoxal (contra a doxa, contra a opinião comum) de Heidegger,
que o homem em 25 séculos não aprendeu a existir --- o filho de Deus
(teologia), o filho do macaco (biologia) e o filho de pai e mãe
(psicanálise\footnote{Ernildo Stein denomina ``encurtamento hermenêutico'' a operação
fundadora do paradigma do Cuidado, a saber, o desligamento do homem
tanto de Deus quanto da Natureza, não incluindo, porém, a destituição da
filiação.} ) ---, ainda não se tornou o filho
da distância instauradora nem morou no mundo com"-os"-outros enformado
pela angústia. Acrescento esse longo parênteses porque, a partir da
biografia de Heidegger escrita por R.
Safranski\footnote{Ainda que não seja polpuda em fontes, polpuda academicamente, fornece um
retrato muito mais vivo de Heidegger que a do industrioso H\,Ott.} , permanece a ideia (já explicitada
nos cursos que Heidegger proferiu no final dos anos 20) de que o
pensamento heideggeriano pode/deve ser lido como uma filosofia
prático"-pedagógica de incitamento e conversão à finitude, e que todo seu
engajamento e ardor revolucionários tinham a ver com uma luta contra o
homem"-sintoma ou com o que eu estou chamando de homem do Dentro.

Fechado o parênteses, é necessário voltar ao conceito, àquela primeira
modalidade da fala na segurança do Dentro. Até o presente momento,
permaneceu a ideia de que no dizer teórico vigora o amparo e a segurança
do mundo. Ele enuncia ``realidades'', atém"-se à unidimensionalidade do
dado consumado e visa ``dizer"-lhe a cara''. É~uma fala atenta ao
manifesto e que esquece, na hipnose da coisa manifesta, do movimento de
eclosão e da manifestabilidade enquanto tal: a coisa está disponível ao
apossamento conceituai de tal maneira que esse aniquila qualquer outra
dimensão que pudesse ser nela vislumbrada. Mas o que pode ter a captação
teórico"-proposicional de um ente com o espaço da arte? Não é necessário,
antes de explicitar a natureza do conceito, investigar a metáfora e a
palavra plena do poeta tradicional? Afinal, mesmo o senso comum pretende
saber que a mulher do artista não é essa descrita até aqui, não é um
bípede sem plumas com determinada organização anatômica, não é uma
fatualidade meramente constatada na percepção, mas a mulher
``metaforizada'' e ``analogizáda''. Não está ensimesmada e reclusa na
própria presença, mas se liga lúdica e significativamente a outros
entes, ultrapassando"-se e vinculando"-se a coisas variadas e insuspeitas.

Muitos já fizeram notar e já puseram em combate o gelo solitário da
coisa apoderada e o calor dos entrelaçamentos e dás correspondências.
Fizeram até derivar a monotonia do conceito (o aedo de Thanatos) a
partir da interrupção e do controle na exuberância erótico"-vinculante da
metáfora (tal interrupção é garantia da sociabilidade comunicativa e da
gregariedade). Assim, quando um poeta e crítico cubano, escrevendo à luz
de velas na sacada, redescobre o parentesco da mulher co'a lua, ele se
enche de encantamento! Pensa que é maravilhoso continuar sendo um
prisioneiro do sonho e se regozija do privilégio de ainda não ter se
tornado um ente de razão nem ter se alienado na frieza de uma ``pulsão
escópica''. Cultor erudito do surrealismo e dos mistérios do
inconsciente freudiano, decide elaborar sua ``teoria estética'' a partir
do pequeno Hans: sabe que quando esse garoto ``via'' um ``cavalo'', ele
não via aristotelicamente uma forma de quatro patas, não via a
``cavalidade do cavalo'' (Joyce), mas sentia medo do pai. Ora, deve
haver então um tipo de acesso ao ente que não o encontre atomizado pela
percepção, mas entrelaçado e amarrado aos outros entes. Desse modo, o
poeta cubano descobre alguns comentadores de Ser e Tempo e fica sabendo
que a imagem do homem"-vidente e do mundo como sistema de coisas é
substituída por aquela do Dasein (interpretante"-compreensor) e do mundo
como sistema de signos\footnote{Estou fazendo menção aqui a algumas leituras equivocadas de Ser e Tempo
--- uma certa recepção neopragmática, o chamado pós"-modernismo italiano
(``pensiero debole''), e pessoas que nietzscheanizam o instrumento de
Ser e Tempo. Dou dois títulos emblemáticos: 1. Thomas Wilson --- O
Modelo do Texto como Modelo do Pensamento de Heidegger, uma
Interpretação Funcionalista e 2. Caminero --- Dali Modello dei Essere
como Cosa ali Modelo dei Essere como Texto.} : assim está feita a
passagem da visibilidade à legibilidade. O~dizer metafórico fica
remetido à condição primeira do ser"-no"-mundo, a saber, a de vincular
significativamente os entes, encontrá"-los juntos na luminosidade da
significação. Se o ver ocular percebe realidades externas, no ver
hermenêutico o \emph{Dasein} tem um mundo (não separado) que é como o
espelho de si mesmo. O~mundo é um caráter do \emph{Dasein,} segundo a
célebre frase de \emph{Ser e Tempo.} Assim, expandida a tese do encontro
instrumental na direção de um pan"-interpretacionismo, a frase de
\emph{Ser e Tempo,} ``A montanha é pedreira, o rio é represa, o vento é
vento nas velas etc.,'' poderia seguir com, a mulher é a lua e o mel, eu
a descubro no namorar etc… Essa homologação é insustentável, mas
o que interessa é mostrar que tanto no instrumento como na metáfora
vigora o amparo e a segurança do mundo. O~homem, em ambos os casos, já
acolheu um sentido, vinculou"-se a ele e encontrou um mundo onde
manter"-se. Tanto o pequeno Hans quanto o poeta metafórico não tocam a
região da arte e do dizer frágil: o cavalo de um e a mulher do outro
estão disponíveis num sentido e numa metáfora. Eles não os encontram na
estranheza demoníaca indisponível (Heidegger, 1979), mas os encontram no
medo e no amor que são afetos do Dentro. Franz Kafka faz \emph{poesia da
aparição e não poesia da metáfora;} seu afeto não gruda na coisa
descoberta, isto é, não se trata de sentir um afeto mas de sentir um
metassentir, na medida em que o afeto é o mistério dele ser. E é aí que
Kafka se mantém! A metáfora e o instrumento substituem o real pelo
possível, mas não tocam o impossível! Isso significa que enquanto nos
ativermos ao ente manifesto como instrumento ou disponível na metáfora,
estaremos deixando de lado o movimento que nos confia o favor dos entes.
Ater"-se ao ente já revelado é esquecer a eclosão e a manifestabilidade
como tal. No caso da mulher do bonde, em que não há nenhum fascínio nem
encantamento, nenhuma metáfora nem o menor sinal de apelo erótico, vemos
que Kafka não é captado"-capturado pela presença da mulher, mas que ele
é, justamente, interpelado para o além (ou aquém) dessa própria
presença; e é isso que o intriga. E~isso é, obviamente, o que venho
chamando de Exposição ao Fora, e que poderia ser nomeado
heideggerianamente: o homem existe convocado pela diferença ontológica e
a presença acena para além de si mesma, para a dimensão do ocultamento
(Heidegger, 1987 --- ``Diálogo com o Japonês''). Trata"-se de escutar um
chamado (mais rigoroso que o da voz de Ser e \emph{Tempo} §56) para se
abrir para além da presença do ente e nomear (dizer) a coisa de uma tal
maneira que a não"-coisa apareça também, e simultaneamente. Por isso, no
dizer poético, vibra a fragilidade das coisas e elas passam a ter quase
o mesmo estatuto que o \emph{Dasein} próprio e finitizado de \emph{Ser e
Tempo.}

Num momento em que a trituração e o desgaste das coisas e dos homens
parece ``ininterrompível'', e estranhamente nem mesmo
percebido\footnote{Eu próprio, tendo trabalhado um ano e meio numa empresa de Recursos
Humanos, notei que ali a compreensão do ser do outro como estoque de
serviço e como fornecedor de trabalho jamais encontrava resistência nos
entrevistados; eles se autocompreendiam precisamente a partir dos
dispositivos técnico"-psicológicos da empresa, isto é,
ser"-dotado"-de"-qualidade"-x, estar apto para tal tarefa, sendo"-lhes tudo
isso absolutamente natural, normal e inquestionável.} , a região inútil e vagabunda do
poema talvez seja (ainda) aquela capaz de salvaguardar as coisas e
nomear a violência constitutiva do homem"-de"-Dentro. Trata"-se de uma
violência mais violenta que aquela da bala, do revólver e do estupro,
que são as únicas violências (violência positivista, empírica) que a
sociedade contemporânea parece poder enxergar. Entretanto, o ínfimo
gesto de um médico, o modo dele relacionar"-se com o diagnóstico do
paciente, por exemplo, o olhar de um psiquiatra ou a palavra de um
psicanalista podem conter um assassinato tão poderoso quanto o
positivista (aquele relatado nos jornais e noticiários); mesmo o
pressuposto básico do cientismo que hoje regula a vida dos homens, a
saber, de que somos um processo natural e de que a morte é apenas um
evento na linha cronológica, cega completamente a prática médica para o
fenômeno da fragilidade ontológica do ser humano. E, como fazer essa
fragilidade ontológica aceder à palavra? Minha posição é a de que isso
não acontece no caso do poeta tradicional nem no do homem de Ser e
Tempo: ambos, como tentei mostrar, encontram o ser disponível daquilo
que há (``o was es gibt'' de Tempo e Ser ou o ``\versal{II} y a'' de
Appolinaire), mas justamente isto ``não como algo indisponível, aquilo
que se aproxima como estranho, demoníaco''\footnote{Protocolo do Seminário sobre a Conferência ``Tempo e Ser'' (Heidegger
--- 1979, p\,284).} . O~dizer assentado no fenômeno do mundo encobre aquele que diz o acontecer
original do mundo. No uso do instrumento e na palavra metafórica (ou na
apreensão estética), o si mesmo ôntico mundano é redeterminado e
confirmado, enquanto na exposição ao Fora (risco do desabrigo) o si
mesmo se desdetermina e o mundo deixa de ser o lugar do encontro com o
meu próprio rosto incessantemente repetido. Vale dizer que, para a
poesia da aparição, o poder do desejo (bem como o da razão) também
aniquila a coisa e que, para compreendermos o texto kafkiano, é
necessário alcançar uma posição onde o ser do ente não está mais
determinado metafisicamente como presença, presença que, na modernidade,
continua remetida ora à abertura da razão (Kant) ora à abertura do
desejo (Freud).

Certa vez, Kafka escreveu que qualquer frasezinha escrita por ele era
literatura. Provavelmente porque ele morava na região da arte. Em
28--8-1904, numa carta ao seu amigo Max Brod, Kafka escreveu:
``{[}…{]} quando abria os olhos depois de uma breve soneca, ainda
pouco seguro de minha existência, ouvi minha mãe perguntar da sacada com
tom natural: que faz você? E uma mulher respondeu do jardim: `Estou
lanchando no gramado.' Então me surpreendi com a firmeza com que as
pessoas sabem levar a vida''\footnote{Passagem citada no livro de Marthe Robert (1982, p\,53).} . Se Kafka ao
menos nos ensina que se somos do mundo não pertencemos inteiramente a
ele, então \emph{é} necessário, num gesto de gratidão, interromper
\emph{o processo} contra ele, a saber, o de tentar nomeá"-lo e capturá"-lo
a partir da teoria sociológica (como se ele fosse um rodapé das grandes
teses weberianas acerca da burocratização do mundo) ou da teoria
psicológica (como se ele fosse um ``caso psíquico''). Justamente ele que
sempre esperou uma palavra liberada, uma palavra de um rosto dirigida a
outro, uma palavra que não viesse da argamassa do universo acontecido,
justamente ele, diante da tentativa de cooptação da sua questão ---
questão infinitamente aberta e turbulenta --- pela estreiteza dos
saberes, teria de recuar para o seu canto e proteger"-se com seu humor
maravilhoso\footnote{Sobre esse aspecto da criação kafkiana, ler o interessante livro de
Michel Dentan: Humour et Création Littéraire dans Voeuvre de Kafka
(1961).} . Teria de fazer isso quando o
melhor caminho seria o inverso: o de, a partir de Kafka e da seriedade
da literatura, reinventar não só a teoria social e a teoria literária
mas, principalmente, a teoria psicológica.

\subsection{\versal{II}. Ser e Tempo \emph{e o Fora}}

Antes de concluir este texto, devo esclarecer o uso que faço da noção de
Fora, uma noção aparentemente exógena ao pensamento de Heidegger. O~fato
de essa noção não ser utilizada explicitamente por Heidegger não
significa, entretanto, que ele não a tenha pensado. Não é tarefa difícil
mostrar a ocorrência de pelo menos uma metáfora espacial em Ser e Tempo.
Se relembrarmos rapidamente o conteúdo desse livro, veremos que
Heidegger, de início, mostrou que o ente intramundano já está sempre
revelado, que estamos familiarizados com ele, sendo, inclusive,
desnecessário que o filosofar comece por uma questão gnoseológica e,
entretanto, no curso da exposição do Tratado, Heidegger destitui essa
familiaridade com o mundo, esse enraizamento do encontro na disseminação
do sentido, para fundá"-lo na falta de raiz e no desencontro. Essa
``tirada de chão'' --- e Heidegger era, segundo o depoimento de Löwith,
tão bom nisso quanto um mestre taoísta --- como sabemos, acontece
explicitamente na famosa frase do §40: ``O Ser"-no"-mundo tranquilizado e
familiarizado é um modo da estranheza (Unheimlichkeit) do Dasein e não o
contrário. O~não sentir"-se em casa (Das Un"-Zuhause) deve ser
compreendido existencial e ontologicamente, como o fenômeno mais
originário''. Essa sentença introduz em Ser e Tempo aquela estranha
movimentação que será o próprio coração da obra, afinal, segundo ela,
``o já"-estar"-lá"-fora"-junto"-ao"-ente'' assegurado pelo acesso instrumental
(superação da filosofia da consciência e de todas as suas faculdades, o
prescindir da interioridade do ente sujeito etc. etc.) aparece como um
\emph{Estar Dentro} em relação ao qual há um Fora. E, mais do que isso,
segundo a célebre passagem, esse Fora, essa distância
(não"-familiaridade) tem um primado sobre a proximidade do Dentro. O
\emph{Dasein} é, mais originariamente, relação a esse Fora; ele é mais
íntimo da distância que da familiaridade. Para meu propósito de
legitimação do uso do termo Fora, basta encontrar, nos parágrafos
dedicados à extimidade (ex"-timo = íntimo do estranho), isto é, nos
parágrafos 54 a 64 de \emph{Ser e
Tempo}\footnote{Páragrafos sobre o quem do Dasein, a culpa ontológica e a estrutura do
Cuidado.} \emph{,} alguma sentença recheada com
metáfora espacial. Ora, logo de início, no §55, Heidegger escreve: ``O
chamado vem de longe (da lonjura) e chama para longe (lonjura)
\emph{{[}aus der Ferne in die Ferne{]},} só é atingido pelo chamado quem
se quer recuperar''. Nessa passagem, fica claro que a recuperação de si
equivale à expropriação do perto/dentro e a um namoro/amigamento do
Fora. \emph{Ser e Tempo} reconhece o Fora, embora ele só tenha cidadania
no silêncio. Não há palavra para o instante (\emph{Augenblick)} nem
lugar para o poeta.

\subsection{\versal{III}. Fora e Clínica}

Como não acredito que o amigamento do Fora possa ser ensinado,
``desconfio'' um pouco de um preceito técnico metatécnico que
deliberasse acerca da manutenção do desconhecido como receita para
evitar o uso fetichista e objetivante da teoria. Penso, com Heidegger,
que a experiência que revela o caráter intrinsecamente sintomático de
toda teoria não é uma experiência ensinável; ela acontece ou não. Dizer
a alguém num instituto de formação profissional: ``primeiro você vai
estudar direitinho a teoria do aparelho psíquico e das posições de
Klein, depois vai decorar todas as fases do amadurecimento em Winnicott
ou as estruturas de Lacan, e feito isso você precisa saber que quando
estiver escutando o outro você não o escutará a partir de nada disso,
mas do lugar vazio que a angústia cavou''… Então, num passe de
mágica, veríamos milhares de psicólogos e psicanalistas inteiramente
identificados com a ordem do mundo falando do indizível, decorando
frases do Levinas e até mesmo buscando os livros de Maurice Blanchot, e
pessoas que jamais mantiveram uma relação tensa e desassossegada com o
próprio ser começariam a dizer que o filósofo Martin Heidegger levantou
a interessante questão do sentido do ser.

Obviamente isso é uma caricatura, mas como tanto as universidades,
exércitos do saber bem e do dizer bem, quanto as instituições de
formação desconhecem inteiramente o Fora, elas apenas reproduzem a
formação de poder percebida por Kafka, chamada homem"-metafísico. Não
quero me estender nessa crítica que poderia se prolongar muito, pois
creio bastante na ideia de um terapeuta, embora não consiga pensá"-lo no
quadro do homem"-profissão. Grosso modo, vejo o terapeuta como alguém
além do moralismo e da psicopatologia. Assim como o vírus é a forma
contemporânea do demônio (Canetti), a psicopatologia é a versão
cientificada da moral. Não trabalha com transferência e
contratransferência, pois essas noções encobrem a possibilidade do
encontro ontológico\footnote{Tais noções remetem o sujeito de volta ao intrapsíquico. Se Nietzsche
desfez a mentira do mundo eterno, Heidegger desfez aquela, mais
persistente, do mundo interno.} , assim como a
psicopatologia encobre a singularidade. Parafraseando Zeljko Loparic,
vejo o terapeuta como aquele que propicia que o outro chegue a si mesmo
como alguém que surge no espaço"-tempo da configuração resplandecente de
tudo que é, a partir de uma dimensão de retraimento ou, como diria
Winnicott, a partir de uma solidão intransponível por qualquer relação
de objeto.

\begin{center}* * *\end{center}


Kafka se mantém na tensão da diferença ontológica, no ponto da
intersecção Dentro/Fora que é a aparição, e o aforismo subsequente
(embora metafórico!) contém a melhor epígrafe para o que está pensado em
\emph{Ser e Tempo}:

\begin{center}\emph{Pois somos como troncos de árvores na neve. Aparentemente eles
jazem soltos na superfície e com um pequeno empurrão deveria ser
possível afastá"-los do caminho. Não, não é possível, pois estão
firmemente ligados ao solo, mas veja, até isso é só aparente.}\end{center}
 \begin{flushright}\emph{(As Árvores)}\end{flushright}


\section{Bibliografia}

\begin{itemize}
\item
  \versal{ANDERS}, Günther. 1969: \emph{Kafka: Pró e Contra}. São Paulo,
  Perspectiva.
\item
  \versal{BLANCHOT}, Maurice. 1987: \emph{O~Espaço Literário}. Rio de Janeiro,
  Rocco.
\item
  \_\_\_\_\_\_\_\_. 1948: \emph{L'Arrêt de Mort}. Paris, Gallimard.
\item
  \_\_\_\_\_\_\_\_. 1980: \emph{L'Écriture du Désastre}. Paris,
  Gallimard.
\item
  \_\_\_\_\_\_\_\_. 1984: \emph{O~Livro por Vir}. Lisboa, Relógio
  d'Água.
\item
  \_\_\_\_\_\_\_\_. 1997: \emph{A~Parte do Fogo}. Rio de Janeiro, Rocco.
\item
  \versal{BOURDIEU}, Pierre. 1989: \emph{A~Ontologia Política de Martin
  Heidegger}. Campinas, Papirus.
\item
  \versal{CALASSO}, Roberto. 1997: \emph{Os 49 Degraus}. São Paulo, Companhia das
  Letras.
\item
  \versal{CANETTI}, Elias. 1983: \emph{Massa e Poder}. São Paulo, Melhoramentos.
\item
  \_\_\_\_\_\_\_\_. 1988: \emph{O~Outro Processo --- As Cartas de Kafka
  a Felice}. Rio de Janeiro, Espaço e Tempo.
\item
  \versal{DENTAN}, Michel. 1961: \emph{Humour et Création Littéraire dans Voeuvre
  de Kafka}. Genéve, Librairie E\,Droz.
\item
  \versal{GABEL}, Joseph. 1953: ``Kafka, Romancier de l'Aliénation''.
  \emph{Critique}, n.78, Novembre. pp\,949--960.
\item
  \versal{GIDDENS}, Anthony. 1991: \emph{As Consequências da Modernidade}. São
  Paulo, Unesp (22 ed.).
\item
  \versal{GOLDMANN}, Lucien. \emph{Lukács y Heidegger --- hacia una filosofia
  nueva}. Buenos Aires, Amorrortu. s/d
\item
  \versal{HEIDEGGER}, Martin. 1992: \emph{Les Concepts Fondamentaux de la
  Métaphysique}. Paris, Gallimard.
\item
  \_\_\_\_\_\_\_\_. 1979: ``Tempo e Ser'', \emph{Conferências e Escritos
  Filosóficos}. Os Pensadores, São Paulo, Abril Cultural.
\item
  \_\_\_\_\_\_\_\_. 1987: \emph{De Camino al Habla}. Barcelona,
  Ediciones del Serbal"-Guitard.
\item
  \_\_\_\_\_\_\_\_. 1971: Qu'est"-ce qu'une chose? Paris, Gallimard.
\item
  \versal{KAFKA}, Franz. 1991: \emph{Contemplação e O Foguista}. São Paulo,
  Brasiliense.
\item
  \_\_\_\_\_\_\_\_. 1993: \emph{Contos, Fábulas e Aforismos}. Rio de
  Janeiro, Civilização Brasileira.
\item
  \_\_\_\_\_\_\_\_. 1997: \emph{A~Metamorfose}. São Paulo, Companhia das
  Letras.
\item
  \_\_\_\_\_\_\_\_. 1986: \emph{Carta ao Pai}. São Paulo, Brasiliense.
\item
  \_\_\_\_\_\_\_\_. 1997: \emph{O~Processo}. São Paulo, Brasiliense (7ª
  reimpressão).
\item
  \versal{KANT}, Immanuel. 1986: \emph{Crítica da Razão Prática}. Lisboa, Edições
  70.
\item
  \_\_\_\_\_\_\_\_. 1974: ``Prefácio à Primeira Edição da Crítica da
  Razão Pura'', \emph{Textos Seletos}. Petrópolis, Vozes.
\item
  \versal{KOHUT}, Heinz. 1973: ``The Future of Psychoanalysis'', \emph{The Search
  for the Self}. Vol \versal{II}. New York, International Universities Press Inc.
  p\,684.
\item
  \versal{LOPARIC}, Zeljko. 1995: \emph{Ética e Finitude}. São Paulo, \versal{EDUC}.
\item
  \versal{MARX}, Werner 1971: \emph{Heidegger and the Tradition}. Evanston,
  Northwestern University Press.
\item
  \versal{NUNES}, Benedito. 1986: \emph{Passagem para o Poético}. São Paulo,
  Ática.
\item
  \versal{PELBART}, Peter P\,1989: \emph{Da Clausura do Fora ao Fora da
  Clausura}. São Paulo, Brasiliense.
\item
  \versal{PIGLIA}, Ricardo. 1987: \emph{Respiração Artificial}. São Paulo,
  Iluminuras.
\item
  PÖ\versal{GGELER}, Otto. 1986: \emph{El Camino del Pensar de Martin Heidegger}.
  Madrid, Alianza Editorial.
\item
  \versal{ROBERT}, Marthe. 1982: \emph{Franz Kafka o la Soledad}. México, Fondo
  de Cultura Económica.
\item
  \versal{SAFRANSKI}, Rüdiger. 1996: \emph{Heidegger et son Temps}. Paris,
  Grasset.
\item
  \versal{STEIN}, Ernildo. 1988: \emph{Seis Estudos sobre ``Ser e Tempo'' (Martin
  Heidegger)}. Petrópolis, Vozes.
\end{itemize}
