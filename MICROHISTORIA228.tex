\chapterspecial{Micro"-História}{}{}
 \epigraph{É um erro axiomático definir a arte por cópias: a vida, compreendo"-a sem
cópias; uma situação nova, um caráter novo encontrado na vida seria
eternamente incompreensível se as cópias fossem necessárias.
\versal{MACEDONIO} \versal{FERNÁNDEZ}}{\versal{MACEDONIO} \versal{FERNÁNDEZ}} 

\section{Micro"-História I (Mulher na Rua)}

Os quintais de domingo e essas folhas secas depositadas sobre o solo. Um
passante, aquém dos arrazoados do mundo, se admira e se detém. Ali, mais
adiante, um velho portão de ferro e o sol iluminando o muro e as bordas
da calçada. A~casa está desabitada, não sabemos para onde foram aqueles
tantos que ali moravam. Passo pelas folhas secas com meus ombros
esquentados; abro o portão e vejo a rua. Uma rua de domingo, nenhum
veículo ou ruído.

Passa um velho embriagado e não me nota. Logo a seguir uma moça de
traseiro empinado e o rosto excessivamente adornado de pinturas…
Seu andar é desenvolto apesar dos saltos, muito altos, e seu traseiro
parece ser o seu centro mais legítimo, sua vitória mais definitiva sobre
a terra e o Desconhecido. Ela o movimenta organizando todo seu corpo em
função da monótona dança daqueles quadris. Sua facilidade em atravessar
a rua, em não notar as folhas e o mistério desses dias faziam dela uma
``coisa"-andarilha''. O~modo, insisto, absurdamente desenvolto com que
cruzou a cracatoa do asfalto, sem se impressionar com a brisa e com a
calçada (cheia de folhinhas secas), sem se interrogar pelo seu caminho,
este modo, absolutamente imerso no seguro, era o modo que eu tinha
perdido e do qual não sentia nostalgia.

De repente passou um carro dirigido por um fulano como ela. Aproximou"-se
e ela acelerou a dança pendular dos seus quadris até que o carro
afastou"-se velozmente.

Ela, então, retomou --- traída em seu anseio mais necessário --- o
pé"-pós"-pé e veio esmagando as folhas secas sem escutar os estalidos tão
crocantes. Havia perdido um pouco da sua desenvoltura e do seu
asseguramento. A~bunda, o seu ponto"-gravitacional mais valioso, a sua
pátria"-portátil, estava, agora, relegada a velhas periferias…

Mas isso só duraria até o próximo carro!

\section{Micro"-História \versal{II} (O Mar e a Dor)}

\begin{flushright}\emph{Para Angela}\end{flushright}


Certa época, já com 28 anos, tentei morar novamente com minha mãe. Ela
alugara uma casa no litoral. Casa belíssima em cima de uma montanha. Da
sacada, podia"-se ver lá embaixo: ver, sentir, tocar na distância
misteriosa de um litoral recortado, cheio de reentrâncias, azul e
brilhante. Era mais forte que um postal de alguma Grécia, Trieste ou
Croácia imaginária. Da sacada eu via aqueles mares e era acossado por
uma loucura: nenhuma história dizia aqueles mares, nenhum relato os
localizava neste mundo, e me parecia impossível que alguma geografia
conseguisse dizer o lugar onde eu morava.

Minha mãe estava preocupada com a minha sanidade, e me dizia: ``Meu
filho, esse negócio de você ficar encantado ao ver cair a folha ou ao
passar uma bela mulher é uma coisa natural que qualquer homem sente.''
Eu tentava responder: ``Não são as mulheres ou as folhas. É~tudo. É~aquele mar. É~o fato de ele ser.'' A presença pura podia me devorar, e
eu não tinha verbo algum para domesticar aquele irromper. Passei os
primeiros dias assustado com o risco de submergir completamente. Ficava
sentado na sacada, e o mar lá embaixo ora era translúcido, cheio de
pontos de cristal, ora era sombrio e escuro feito um pesadelo egípcio.

No quinto dia, quando decidi partir daquele lugar, chegaram alguns
parentes, amigos e conhecidos. Primos e tios falavam e passeavam
familiarmente fazendo o elenco das virtudes do lugar. Com muita
desenvoltura olhavam o mar e examinavam a arquitetura da casa, sua
posição nas rochas, a ``janela para a bela paisagem'' etc. Um amigo de
minha mãe falou da distância --- 120\,km --- da cidade até aquele belo
``condomínio marítimo'' onde eu e ela iríamos morar. Dando"-me um tapinha
nas costas, perguntou"-me se eu já havia providenciado um ``windsurf''.
Minha tia, sempre mais esnobe e afetada, perguntou se não havia risco de
farofeiros. No caminho passara por uma favela e estava preocupada. Em
seguida mostrou uma reunião animada na sacada de uma outra casa: amigos
tinham vindo para um churrasco, e ela me perguntou se eu já fizera
amizades… Essa pergunta era uma pergunta hipócrita, pois minha
tia sabia da minha desordem interior e da ``maldição'' de minha
singularidade, e, é óbvio que ela não me descrevia nesses termos, mas em
outros, apoiados no ``saber médico''. Sua verdadeira preocupação, como
vim a saber depois, era se eu não iria ``jogar futebol com os
favelados'', se eu compreendia que pertencia ao condomínio dos ricos.
Era disso que ela iria tratar com minha mãe.

Eu acompanhava tudo aquilo, percebia os chamados ``jogos psicológicos''
com bastante lucidez. Meu saber abstrato e astuto ``conhecia os costumes
e as almas e esse dialeto de alusões que toda agrupação humana vai
urdindo'' (Borges). Mas eu estava aflito: eles eram embaixadores do
real. Todas as suas histórias eram oriundas dessa província. E~eu não
conseguia me identificar com elas, ler meu ser e o do lugar ---
contê"-los --- a partir delas! Falavam da paisagem, do windsurf, e nesse
quinhão de metáforas pareciam exorcizar o mar. Eu escutava os
significados e eles me pareciam demasiado frágeis para romper com o meu
sentimento do lugar, a potência selvagem que havia ali. Sofria então,
tanto com a força do estado sem palavras que permanecia me invadindo e
me deixando solitário, quanto diante da fraqueza terrível das histórias
onde os visitantes viviam. E~a fragilidade de uma coisa não conseguia
encobrir a monstruosidade da outra… Vaguei pelo condomínio,
vasculhado por não sei que peso, na tentativa de dizer o mar… Ah,
se alguma voz soubesse iluminar esse milagre…

Quando retornei, à noite, os visitantes já tinham partido. Sentei"-me na
sacada e soube duas coisas: não saberia ser poeta e nem viver no poderio
da aldeia dos homens. Na noite esparramada ruminei estratégias para
escapar de um destino problemático.

No dia seguinte, enquanto arrumava minhas malas, não sabendo para onde
ir, encontrei o seguinte papel no quarto de minha mãe. Nele estava
escrito: Notas para compreender meu filho: ``Juliano não acredita mais
no grande mito da origem, não acredita que essa seja a história de todos
nós. Provavelmente eu não sou mais uma mãe e estou apavorada por ter de
conviver com ele. Fica mudo na sacada e eu não acho mão humana que o
traga à brecha de luz''.

Dentro do ônibus, fugindo do litoral, reparei no meu vizinho de
poltrona. Enquanto ele era uma espécie de mônada seletiva, recolhida na
própria província, tendo múltiplos padrões de medida para compreender o
universo, eu era acossado pela vertigem de tudo que é, pelo assombro de
qualquer visão. Não tinha pontos de regresso ou histórias onde me
recolher. Durante a viagem vi o clarão do sol e o outro, diferente, das
estrelas… Cruzei duas cidades enormes e as casinhas de beira de
estrada, cada uma delas era um enigma devorando meu sentir. Num silêncio
cheio de dúvidas, interrogava tudo sem obter resposta. Soube que o
conhecimento tinha se tornado imprestável e que era necessário precisar
os meus caminhos…

\section{\versal{OUTRAS} \versal{REFER}Ê\versal{NCIAS}}

\begin{itemize}
\item
  \versal{ADORNO}, T\,W\,\& Horkheimer, M\,Dialética do Esclarecimento. Rio de
  Janeiro Jorge Zahar, 1985.
\item
  \versal{ADORNO}, Theodor W\,Teoria Estética. Lisboa, Edições 70, s/d.
\item
  \versal{ANZIEU}, Didier. Beckett et le Psychanalyste. Paris, Mentha/
  Archimbaud, 1992.
\item
  \versal{BENJAMIN}, Walter. Magia e Técnica, Arte e Política. São Paulo,
  Brasiliense, 1987.
\item
  \versal{BERNHARD}, Thomas. Árvores Abatidas. Rio de Janeiro, Rocco, 1991.
\item
  \_\_\_\_\_\_\_\_. O~Náufrago. São Paulo, Companhia das Letras, 1996.
\item
  \_\_\_\_\_\_\_\_. Perturbação. Lisboa, Relógio d'Agua, 1986.
\item
  \_\_\_\_\_\_\_\_. Un Enfant. Paris, Gallimard, 1984.
\item
  \_\_\_\_\_\_\_\_. L'origine. Paris, Gallimard, 1981.
\item
  \_\_\_\_\_\_\_\_. La Cave. Paris, Gallimard, 1982.
\item
  \_\_\_\_\_\_\_\_. Le Souffle. Paris, Gallimard, 1983.
\item
  \versal{BORGES}, Jorge Luis. Obras Completas. Buenos Aires, Emece, 1974.
\item
  \versal{BROCH}, Hermann. Création Littéraire et Connaissance. Paris, Gallimard,
  1966.
\item
  \versal{CANETTI}, Elias. A~Língua Absolvida. São Paulo, Companhia das Letras,
  1987.
\item
  \_\_\_\_\_\_\_\_. Uma Luz em meu Ouvido. São Paulo, Companhia das
  Letras, 1988.
\item
  \_\_\_\_\_\_\_\_. O~Jogo dos Olhos. São Paulo, Companhia das Letras,
  1990.
\item
  \versal{CIORAN}, E\,M\,Oeuvres. Paris, Gallimard, 1995.
\item
  \versal{CHAR}, René. O~Nu Perdido e Outros Poemas. São Paulo, Iluminuras, 1995.
\item
  \_\_\_\_\_\_\_\_. Fureur et Mystère. Paris, Gallimard, 1967.
\item
  \versal{DELEUZE}, Gilles. Conversações. Rio de Janeiro, Editora 34,1992.
\item
  \versal{DUARTE}, André de Macedo. ``O Pensamento à Sombra da Ruptura: Política
  e Filosofia na Reflexão de Hannah Arendt''. Tese de Doutorado.
  \versal{FFLCH}-Universidade de São Paulo, 1997.
\item
  \versal{FERRAZ}, Maria Cristina Franco. Nietzsche. O~Bufão dos Deuses. Rio de
  Janeiro, Relume Dumará, 1994.
\item
  \versal{GOMBROWICZ}, W\,Moi et son Double. Paris, Gallimard, 1996.
\item
  \_\_\_\_\_\_\_\_. Vingt Ans Aprés. Paris, Christian Bourgois Éditeur,
  1989.
\item
  \versal{INGARDEN}, Roman. The Cognition of the Literary Work of Art. Evanston
  Northwestern University Press, 1973.
\item
  \versal{KAFKA}, Franz. Diários. Buenos Aires, Marymar, 1977. Vol. I e \versal{II}.
\item
  \versal{LOPARIC}, Zeljko. Descartes Heurístico. Campinas, Unicamp"-Instituto de
  Filosofia e Ciências Humanas, 1997.
\item
  \_\_\_\_\_\_\_\_. ``O fim da metafísica em Carnap e Heidegger'', De
  Bone, Luís Alberto (org.) Festschrift em Homenagem a Ernildo Stein.
  Petrópolis, Vozes, 1996. pp\,782--803.
\item
  \_\_\_\_\_\_\_\_. ``Origem e Sentido da Responsabilidade em
\item
  \versal{HEIDEGGER}'' Veritas, vol. 44, n21, Março. Porto Alegre, 1999, pp\,  201--220.
\item
  \versal{MELVILLE}, Herman. Great Short Works. New York, Perenial Library, 1969.
\item
  \versal{NIETZSCHE}, Friedrich. Ecce Homo. São Paulo, Max Limonad, 1986.
\item
  \versal{NUNES}, Benedito. O~Drama da Linguagem. São Paulo, Ática, 1989.
\item
  \versal{PELBART}, Peter Pál. A~Nau do Tempo Rei. Rio de Janeiro, Imago, 1993.
\item
  \versal{PAZ}, Octavio. Os Filhos do Barro. Rio de Janeiro. Nova Fronteira,
  1984.
\item
  \_\_\_\_\_\_\_\_. O~Labirinto da Solidão e Post"-Scriptum. São Paulo,
  Paz e Terra, 1984.
\item
  \_\_\_\_\_\_\_\_. Signos em Rotação. São Paulo, Perspectiva, 1971.
\item
  \versal{RICOEUR}, Paul. O~Conflito das Interpretações. Rio de Janeiro, Imago,
  1978.
\item
  \versal{RORTY}, Richard. Contingência, Ironia e Solidariedade. Lisboa,
  Editorial Presença, 1992.
\item
  \_\_\_\_\_\_\_\_. Ensayos sobre Heidegger y Otros Pensadores
  Contemporâneos. Barcelona, Editorial Paidós, 1993.
\item
  \versal{SIMANKE}, Richard T\,A~Formação da Teoria Freudiana das Psicoses. Rio
  de Janeiro, Ed. 34, 1994.
\item
  \versal{STEIN}, Ernildo. A~Questão do Método na Filosofia. São Paulo, Duas
  Cidades, 1973.
\item
  \_\_\_\_\_\_\_\_. Racionalidade e Existência. Porto Alegre, L\&\versal{PM},
  1988.
\item
  \_\_\_\_\_\_\_\_. Seminário sobre a Verdade. Petrópolis, Vozes, 1993.
\end{itemize}
