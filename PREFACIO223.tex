\chapterspecial{Prefácio}{}{Manuel da Costa Pinto}
 

``Aquilo que desaloja é o mais hospitaleiro.'' Essa frase é uma espécie
de \emph{leitmotiv} que, com modulações diferentes, percorre os poemas,
narrativas e ensaios de \emph{Ignorância do Sempre} e assinala a relação
especular que esse segundo livro de Juliano Garcia Pessanha mantém com
sua obra de estreia, \emph{Sabedoria do Nunca}. Essa inversão simétrica,
sugerida pelo irônico jogo de palavras entre os dois títulos, indica
também a transmutação pela qual passa essa meditação poética sobre o
ser. Em \emph{Sabedoria do Nunca}, Juliano havia exposto as fraturas que
se abrem diante de quem é desabitado do mundo, de quem percebe o
assassinato a que somos submetidos pela violência contida nas palavras
que nomeiam e dão um sentido aos entes; em \emph{Sabedoria do Nunca}, a
presença do mundo (com suas cristalizações, suas camisas"-de"-força, suas
calamidades cotidianas, seus campos de concentração domésticos) tinha
como contrapartida a ausência ou a impossibilidade do ser, que apenas
percebia a luz de uma palavra ausente, a palavra poética, que poderia
restituí"-lo a um lugar anterior a seu despedaçamento; na viagem
sonâmbula da personagem Z (do conto ``Deslocamento'') ou nas reflexões
da \emph{persona} filosófica K (do ensaio que fecha \emph{Sabedoria do
Nunca}) estava estabelecido assim um território ficcional que, ao lado
dos aforismos poéticos desse primeiro livro, esboçava uma topologia de
nosso desterro contemporâneo.

Agora, em \emph{Ignorância do Sempre}, Juliano Garcia Pessanha retoma
esse esquema de gêneros literários alternantes (diversos poemas em
prosa, dois ensaios e duas ``micro"-histórias'') para desenvolver sua
cartografia do exílio. Uma vez desfeitas as redes da linguagem e
impugnada a arregimentação da presença, Juliano ``transcende o
criticado'', ou seja, vai além da denúncia da catástrofe metafísica, do
escândalo do ser aprisionado pelo sentido. Num dos ensaios de
\emph{Ignorância do Sempre}\emph{,} Juliano cita Blanchot para afirmar
que aquele que ``perdeu o mundo e a verdade do mundo, que pertence ao
tempo do exílio e do desamparo'' é, paradoxalmente, aquele que pode
\emph{encontrar as coisas}\emph{.} Ou seja, estar fora desse mundo
seguro, nomeado, legislado, é de alguma maneira reencontrar um ser que
antecede a tentativa de dissolução do enigma da existência por perguntas
que já encerram suas respostas. Eis a virada positiva que
\emph{Ignorância do Sempre} realiza em relação a \emph{Sabedoria do
Nunca}\emph{:} ``Aquilo que desaloja é o mais hospitaleiro''.

Mas essa topologia que opõe o pensamento do \emph{dentro} (a metafísica
da presença, mas também a crítica que não consegue ultrapassar a
desconstrução do horizonte identitário do mundo) ao pensamento do
\emph{fora} (que celebra o desassossego e conserva o enigma da pergunta)
tem --- além de sua importância inequívoca para a discussão da filosofia
antimetafísica (sobretudo de Heidegger) --- uma consequência que
assegura para Juliano uma singularidade e uma densidade inauditas dentro
da literatura brasileira. Pois é preciso destacar que ele se afasta da
tendência estetizante que impera de modo absoluto em nossa poesia. Para
além das misérias literárias que opõem os críticos de feição
formalistas, os sociólogos da literatura e os devotos do sublime,
Juliano habita uma literatura que só é literária porque é fiel a seu
mundo --- um mundo que, despedindo"-se da superfície intramundana do
nosso mundo, assinala a força desviante da palavra poética como abalo
ficcional da linguagem e da realidade que seus signos recobrem.

O resultado é uma poesia que não se inscreve em linhagens poéticas ---
embora possamos pensar nas iluminações oraculares de Hölderlin ou René
Char, no hemisfério indizível de Paul Celan ou ainda no ensimesmamento
de Bernardo Soares (o heterônimo de Fernando Pessoa) ---, uma poesia que
não tem pesquisas formais e que oscila entre a abstração do conceito e
uma metafísica feita com ferramentas da vida doméstica, que às vezes
esbarra nos limites do \emph{kitsch} (a exemplo do que acontece com
Clarice Lispector --- seguramente o nome do qual Juliano mais se
avizinha dentro da literatura brasileira); ou seja, uma poesia que não
deve ser julgada pelos instrumentos convencionais da poética e que
justamente por isso ultrapassa esse beletrismo travestido de vanguarda
em que se transformou a poesia contemporânea. Em \emph{Ignorância do
Sempre} (mas também em \emph{Sabedoria do Nunca}), a novidade se produz
não por uma ruptura estética com o passado literário, mas por uma
rachadura filosófica do presente ontológico --- deslocando o eixo de
interpretação do literário, propondo um novo pacto entre autor e leitor
pela constituição de um lugar de vivência, de visitações de sentido, de
acontecimentos cuja duração obedece ao relógio da experiência da
leitura, impregnando o leitor não pela decantação do significado, mas
pelas perfurações que tornam o ser permeável a experiências e enigmas
poéticos sempre renovados.
