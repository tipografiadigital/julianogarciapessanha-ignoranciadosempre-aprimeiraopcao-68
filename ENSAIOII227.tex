\chapterspecial{Ensaio \versal{II}}{}{}
 \begin{flushright}\emph{Ao Gilberto}\end{flushright}


\section{Por uma Nova Topologia da Sanidade\footnoteInSection{Este texto foi lido no \versal{CAPS}-Itapeva (Centro de Atenção Psicossocial) de
São Paulo, em 28 de junho de 1999.} }

\begin{center}\emph{Enquanto a aranha cerzia o frágil arabesco da sua teia, vi que a
imensidão da noite circundante permanecia atravessando todas as linhas
brancas do traçado e que ela passeava dentro da doçura de um abismo.
Lembrei"-me então de uma outra maravilha cujo nome é homem e descobri o
segredo de uma afinidade! Se a aranha faz a teia, o homem tece
biografia. Biografia é a tristeza de não ter podido residir no elemento
negativo: se o homem foi constrangido a abandonar a ``simplicidade da
noite'' pela loucura do nascimento, ele pode, numa rememoração
permanente do oculto, suportar a luz cansada que vigora na passagem pelo
exílio deste mundo.}\end{center}
 \begin{center}* * *\end{center}


Essa imagem contém uma primeira descrição pós"-metafísica do ser humano.
Ela é pós"-metafísica porque remete o acontecer do homem ao enigma da
noite. A~noite é abarcadora! Não porque ela está antes do início da teia
ou, simplesmente, depois dela. Ela é abarcadora porque atravessa
constantemente o traçado, acompanhando a aranha a cada passo do seu
trânsito. A~imagem da aranha e da noite é uma imagem estranha, próxima
da heresia gnóstica, uma imagem"-contrária porque a nossa história, a
história transmitida e acontecida do Ocidente, insistiu em pensar apenas
o elemento da teia e deixou de lado o que está fora dela. Mas se a
metafísica da presença pensou apenas o Dentro, devemos, agora, começar a
pensar o Fora. E~devemos fazê"-lo não porque isso seja apenas uma
novidade ou um pensamento diferente no mercado das ideias. Não: pensar o
Fora não é produzir mais um pensamento para enriquecer o estoque da
cultura, mas operar uma mutação na nossa maneira de existir. Somos,
hoje, eticamente forçados a pensar diferente, porque estamos, pela
primeira vez, numa situação capaz de perceber a violência e a agressão
que dormitavam no pensamento metafísico.

A aranha passeava dentro da doçura de um abismo, vale dizer que, o que
protege o trânsito da aranha é a pureza do enigma, e tão somente ela.
Não há um suporte, uma origem, ou uma metateia possibilitadora. Se o
pensamento metafísico (do Dentro) remete o positivo sempre a uma outra
positividade facultadora, o pensamento aberto ao Fora remete o
arremessar da teia à vigília da noite. Dito em termos filosóficos: o
homem está demitido do fundamento. Porque está demitido do fundamento,
ele não é uma realidade; os entes reais subsistem e têm uma causa. O~homem é sem causa e sem princípio de razão suficiente. Dizer o aspecto,
dizer a forma escultórica da realidade do homem, é cegar"-se, é não ver a
sua existência. Se as coisas têm um rosto"-quididade ou uma essência, do
homem deve"-se dizer apenas que ele pulsa, que ele é uma vibração. O~sentimento de ser real --- se tal proposição fosse possível --- está na
antípoda do sentimento de existir. Por isso, a tarefa do pensamento,
enquanto pedagogia do finito, é conduzir o homem ao lugar da pulsação. É~operar aquilo que Macedonio Fernández chamou de ``sufocamento'' e
``aniquilação'' do ens realissimum, do homem"-altivez. Se estamos hoje
aqui, psicólogos, psiquiatras e psicanalistas, é porque nos tornamos
psicólogos, psiquiatras e psicanalistas, e isso não estava dado antes de
percorrermos o caminho que nos trouxe até aqui. Se ser psicanalista
tornou"-se"-me algo próprio, é porque me enganchei em certas
possibilidades e não em outras. O~encontro aleatório e acidental com um
livro, uma pessoa ou teoria tornou"-se"-me necessário, isto é, enfiei"-me
nessa possibilidade e não em outras. Estar aqui, agora, é não estar em
todas as outras reuniões possíveis, de tal maneira que quando olhamos
retrospectivamente o caminho de nossa teia biográfica, vemos que ele é
apenas o riscado mais acentuado no espalhado imenso das possibilidades
que não sou. Assim, quando o homem altivo nomeia sua realidade, ele
esquece que a noção aparentemente pacífica e primeira de realidade é, na
verdade, derivada e não originária: real é apenas um possível
endurecido! Esse esquecimento ativo do possível não tem nada a ver com
alguma nostalgia psicológica dos preteridos, uma vez que a própria noção
de ``escolha'' não se deixa, aqui, tematizar nos quadros da filosofia da
consciência ou do humanismo voluntarista: o ativamente esquecido,
pensado ontologicamente, é o primeiro furo não assimilado pelo homem
altivo ou, melhor dito, a consistência ontológica do ens realissimum não
pode reconhecer esse primeiro não.

O reconhecimento do primeiro"-não desemboca no reconhecimento da nossa
radical historicidade. Estamos imersos no tempo biográfico, é nele que
moramos, mas o nosso tempo biográfico está imerso no tempo histórico. O~existente não é um ente determinado ou ``influenciado'' pela história,
comó se houvesse, de um lado, o homem, e de outro ``algo exterior''
chamado história, a exercer algum tipo de pressão e domínio sobre um
homem pré"-existente. Não. Nós somos, pura e simplesmente, história; o
histórico é a própria raiz de nosso ser. Mesmo nossos afetos mais caros
ou nossas crenças mais íntimas são, paradoxalmente, o que há de mais
público; são, paradoxalmente, o não"-nosso. Toda a obra de Nietzsche pode
ser lida como um grande soco na altivez humana, na medida em que pôs às
claras o caráter circunstancial e inessencial de todos os afetos
considerados naturais e essenciais. No aforismo 35 do Aurora, ele
escreveu: ``Confia em teu sentimento! Mas sentimentos não são nada de
último, originário, por trás dos sentimentos há juízos e estimativas de
valor, que nos foram legados na forma de sentimentos (propensões,
aversões). A~inspiração que provém do sentimento é o neto de um juízo
--- e muitas vezes de um juízo falso --- e, em todo caso, não de teu
próprio juízo! Confie em seu sentimento --- isso significa obedecer mais
ao seu avô e à sua avó e aos avós deles do que aos deuses que estão em
nós…

Assim, se estamos aqui reunidos, psicólogos, psiquiatras e
psicanalistas, a primeira coisa que devemos ter presente é que durante a
maior parte do tempo não havia psiquiatras, psicólogos e psicanalistas.
Não havia sequer a partícula ``psh determinada objetivamente e o homem
já existia. Devemos sempre lembrar que a teoria jamais dirá o ser do
homem, e isso pela simples razão de a teoria ser um sintoma do homem e,
como toda criação humana, ela traz em si a ``leveza da falsidade''. Por
isso, um olhar furado pelo ``não'' do possível, ao examinar qualquer uma
dessas instituições profissionais (psicologia, psiquiatria,
psicanálise), deve transcender sua aparente concretude ou naturalidade e
interrogar pelo verdadeiramente instituinte: o verdadeiramente
instituinte é sempre uma sentença, uma proposição que busca pacificar a
turbulência da pergunta. Se há igreja e hóstia, é porque alguma vez
determinou"-se o sentido de ser do homem como filho de Deus. Se há um
medicamento psiquiátrico, é porque antes determinou"-se o sentido de ser
do homem como coisa orgânica, como filho da natureza e do macaco, e não
mais como filho de Deus. Se hoje não se questiona o ser do remédio e não
se sabe por que cresce o consumo das chamadas drogas ilegais, é porque
não se põe em crise a determinação correlata do ser do homem como
``entidade neurofisiológica''. Tanto o filho de Deus (Teologia) como o
filho do macaco (Biologia) ou o filho de pai e mãe (Psicologia) são
respostas epocais que o homem deu à constante interpelação do enigma da
sua aparição. São possibilidades, e essas possibilidades atravessam
ainda o homem histórico, mas o enigma, ele mesmo, permanece puro e
inexorável, rasgando incessantemente o manto de tanta resposta possível.
Blanchot dizia que a resposta é a tragédia da pergunta e, com isso, ele
nomeou o paradoxo kafkiano de Odradeck: isto é, o homem nomeia e
responde e, logo em seguida, fica hipnotizado pela resposta que ele
mesmo deu. Por que, ao invés de seguir respondendo, o homem não se
pergunta pela sua demanda de resposta? Por que, ao invés de permanecer
entretido apenas no possível, o homem não se amiga do impossível?
Heidegger mostrou claramente que a infinitude da resposta se assenta no
caráter finito de nossa condição, e sugeriu que, ao invés de tentarmos
encobrir e domesticar o mistério do arremesso, poderíamos, numa
despedida do teórico, passar a celebrá"-lo no poema. O~poema não é um
objeto estético, não é algo belo. O~poema é o nome do lugar da
existência humana, é a morada no desassossego da pergunta, e Heidegger
apontou a possibilidade na qual a fragilidade do homem"-pergunta pode
derrotar a truculência do homem"-resposta.

A perfuração do homem"-resposta, o ultrapassamento da altivez humana, é a
tarefa de uma terapêutica do finito. Macedonio Fernández escreveu que
``é muito sutil, muito paciente, o trabalho para abolir egos, para
sacudir interiores, identidades. Só consegui, em toda a minha obra
escrita, oito ou dez momentos em que --- me parece --- duas ou três
linhas abalam a estabilidade, a unidade de alguém e, às vezes, o próprio
ser do leitor. Entretanto, penso que a verdadeira Literatura ainda não
existe, porque não se dedicou exclusivamente a esse efeito de
desidentificação, o único que justificaria sua existência e que somente
essa Belarte pode elaborar… se em cada um de meus livros consegui
duas ou três vezes aquilo que chamarei, em linguagem coloquial, uma
`sufocação', um aniquilamento na certeza da continuidade pessoal, uma
derrapagem do leitor em si mesmo, é tudo o que quis como meio. Como fim
busco a liberação da noção de morte, a trocabilidade, a rotação, a
alternação do ego…'' Produzir uma ruptura na consistência
ontológica do ens realissimum, a fim de deixar ser a fragilidade do
homem, foi também o problema de Heidegger ao longo do seu caminho. A~insistência na difícil equação, na qual a fragilidade tritura o poder,
atravessa a obra heideggeriana dos anos 20 até o final, e o momento
culminante é precisamente aquele em que Heidegger percebe a necessidade
de abandonar o próprio querer dessa transmutação, pois tanto o querer
como a vontade são, ainda, categorias do poder. Despede"-se da política e
de todo o ativismo revolucionário para pensar a Gelassenheit
(serenidade) e, se aproximando de Hölderlin, busca, com ele, apontar
para onde devemos olhar. Mas mesmo então, mesmo o tema da passagem da
armação (Gestell) para o Geviert (Quadratura), é ainda uma reformulação
da difícil equação em que a fragilidade derrota o poder. Como nomear a
experiência onde o ``tropeço da sombra'' alcança o homem moderno? Como
mostrar ao homem de hoje, ao herdeiro da disciplina da luz, que aquilo
que desaloja é o mais hospitaleiro e que a subjetividade é ainda mais um
cativeiro? Todas essas são questões gêmeas embutidas na equação
heideggeriana. Se, por um lado, é necessário trazer o homem de Dentro
para a proximidade do Fora, por outro, e Heidegger se esqueceu disso, é
preciso também trazer aquele que permaneceu Fora até a proximidade do
Dentro, facultando"-lhe a possibilidade de encontrar os outros, as
coisas, e de permanecer no aberto do mundo, afinal, isso é tudo que nos
foi confiado, o alfa e o ômega de nossa existência. Seria bastante
ingênuo fazer um elogio unilateral do Fora, isso equivaleria a admirar
aqueles seres que ``presos nas profundezas, sumiram de si para sempre''
(Celan). Os Bartleby, os Kasper Hauser, os não"-nascidos de Beckett e
Trakl, aqueles que o discurso ``psi'' chama de autistas, são muitos os
engolfados pelo enigma, os que permaneceram imantados na noite e
recuados do destino, frequentemente por uma recepção pouco acolhedora. É~nosso dever meditar no tipo de encontro capaz de parir o evento, capaz
de puxar um recluso até o lugar da manifestação de tudo o que é. Essa
meditação acerca da saudação implica uma desconstrução do homem de
Dentro, numa contínua ``aniquilação'' do homem atual. O~homem atual, o
homem adaptado de hoje, está numa condição capaz de triturar
completamente o êxtase de um recém"-chegado, de fazê"-lo recuar
inteiramente, possibilitando"-lhe apenas uma existência simulada e
mortificada.

É por isso que devemos pensar o Dentro junto do Fora e o Fora junto do
Dentro, numa disjunção contínua: lá onde está o Fora, que se leve o
Dentro; lá onde saturou o Dentro, que se leve o Fora. Esse lugar, esse
fio de cabelo, essa linha de horizonte é o lugar do homem. A~posição da
sanidade é aquela do umbral. Trata"-se de uma posição difícil: de um lado
há o risco permanente da atração da opacidade e de outro o de tornar"-se
um devir"-ideia, uma mera instalação num mundo de coisas instaladas. A~luminosidade que vigora no mundo contemporâneo é a luminosidade total da
instalação perseguidora. Assistindo a uma entrevista no programa
Planeta"-Xuxa da Rede Globo, fui atingido por um afeto que assinalava o
perigo e a devastação presentes na luminosidade atual. Antes de um
entrevistado poder responder como era beijar em cena, a apresentadora o
interrompeu e descreveu como era o seu beijo real: disse que quando
escolhia alguém para gostar ela ``fazia direitinho'' o beijo, fechava os
olhos, às vezes o abria para observar o andamento e a recepção; enfim,
que ela sabia dar o verdadeiro beijo. Notei, então, que aquela mulher
estava inteiramente confinada no doloroso universo da aparência,
desconhecendo o movimento amoroso da aparição. Um beijo deve conter mais
que um beijo, ele deve ter chegado de um lugar indeterminado, lugar que
o converta em algo além de um mero movimento facial. Quando as coisas já
nem sequer podem mais se manifestar, então se torna urgente abrirmos os
olhos para suspender o assassinato. Devemos fazer a anamnese da nossa
história acontecida, a fim de compreendermos por que estamos enfiados
nessa hora perigosa. O~pensamento heideggeriano funciona como um guia,
pois ele não é mais do que a rigorosa narrativa reminiscente dos tipos
de luminosidade que imperaram ao longo da história ocidental.
Recapitular essa história é apropriarmo"-nos de nosso si mesmo, é
despedirmo"-nos de nosso falso self. Se em Platão todas as coisas se
manifestam à luz da medida da ideia e elas guardam, ainda, o vigor da
manifestação, no caso do beijo instalado, é como se a idealidade
platônica se tornasse concreta, aniquilando a chance de um surgimento.
Quando cessa a espontaneidade e a gratuidade da manifestação, então o
modo da presença das coisas é um vazio mortificado. Que acolhida terá um
recém"-chegado? Haverá nos saudadores, isto é, nas mães, um espaço de
acolhimento em que cesse o gesto e o dizer administrados? Como abrir um
furo no homem contemporâneo e suspender o cenário em que se converte o
nosso mundo? Thomas Bernhard, provavelmente o escritor mais lúcido da
segunda metade do nosso século, mostrou claramente que a sociedade atual
é um imenso absurdo pintado de cor"-de"-rosa, ele mostrou isso com todo
vigor e, entretanto, não pôde transcender o criticado, não pôde dar o
passo para fora. Ele, assim como toda a filosofia e a literatura
``críticas'', permanecem ainda vinculados ao ``material criticado'' como
aquele homem que fica fechado num quarto junto a tudo que odeia.

Se dou exemplos televisivos, é porque não quero dar exemplos de pessoas
consideradas reais e ser taxado de antiético pelos paladinos do bem ou
pelos justiceiros das comissões de ética. Hoje, conforme mostrou
Heidegger (e Fernando Pessoa já havia feito o mesmo), o bem do bem é
intrinsecamente mal e devemos repensar radicalmente a noção de ética.
Persistir invocando a divina providência, persistir invocando a vetusta
lei moral, depois de tudo o que já sabemos sobre o século \versal{XX}, é uma
aberração que só encontra legitimidade na insanidade do bom senso. O~exemplo televisivo, obviamente, não é oriundo da televisão. A~televisão
é apenas uma janela através da qual pessoas já deslocadas podem observar
a essência da nossa cultura, os traços mais marcantes do que hoje é. E~bastam alguns exemplos, não é preciso tanta sociologia e tanta
antropologia francesas. Os livros interessantes de Baudrillard, por
exemplo, podiam caber num só livro, pois eles são apenas exemplificações
daquilo que Heidegger, avesso a exemplos, havia pensado com o termo
armação (Gestell), 30 anos antes desse tipo de literatura tornar"-se
moda. Se não podemos ser como Buda que em só três passeios percebeu a
essência inteira da vida, podemos, com estudo e diálogo, terminar por
compreender a estância em que estamos metidos. Trata"-se sempre de uma
estância, pois o modo da manifestação das coisas é historial, o que
equivale a dizer que só compreenderemos o beijo"-instalação da
apresentadora se percorrermos, retroativamente, a ontologia de
Nietzsche, a crítica da Razão Pura, as meditações de Descartes e assim
sucessivamente, até o estopim do começo grego. Acordar para o sentido
histórico, para a doação metamórfica do ser, é desfazer em nós o senso
comum e a sua credulidade tomista. O~ens realissimum sofre de
historiofobia e de filotomismo, pois acredita na perenidade do ser e na
igualdade mesmificante de tudo o que é. Assim, ao mudar de canal, vejo
uma entrevista com um publicitário que se afirmava um Leonardo e um
Piero de la Francesca de hoje, afinal, se a igreja precisava de
marketing, a coca"-cola hoje também tem o seu Rafael. Esse homem dormido
poderia discorrer longamente acerca da universalidade da obra de Kafka,
como se ela pudesse ser compreendida por um monge medieval, e pensaria
que o beijo"-Xuxa poderia ter sido dado por Heráclito ou por um
recém"-nascido. Como mostrar a esse homem, a esse homem que discorria
incessantemente sobre a beleza dos cenários (cenários"-cidades,
cenários"-pessoas e o grande cenário"-História), que a sua ocularidade
estetizada (que nada engaja) é, ela própria, o modo de ser do homem
quando a presença das coisas ficou oca e o surgimento toca a
desaparição? Como conduzir esse homem até a pulsação? Como deslocá"-lo?
Ao encontrar um homem de Dentro bato duas vezes em sua testa como se
estivesse diante de uma porta e então pergunto: ``Há alguém aí, tem
alguém morando aí?'' Quando a resposta não chega, fico dando voltas em
torno dele, pois confio, ainda, que o bruxuleio da chama deve estar em
algum canto daquele corpo. Num gesto desapercebido, na pontinha da
orelha ou na pinta atrás da nuca pode estar a angústia. E~a angústia é a
esperança do homem! Ser e Tempo mostrou, com uma certeza mais certa e
mais rigorosa que a certeza dos teoremas, que a angústia é o júbilo que
assinala a presença de um homem. A~angústia é o incontornável da
psiquiatria e a medicalização crescente jamais domesticará o terremoto
da angústia pela simples razão que a busca desenfreada da medicalização
é, ela própria, possibilitada pela presença da angústia. Assim, quando
um homem instalado, um homem de Dentro, é brindado com a sorte de
tropeçar numa dor ou num sintoma e chegar até a um consultório ou a um
divã, o terapeuta deve se alegrar e ver que nesse sintoma e nessa dor
estão o pedido de passagem e o de chegada do homem verdadeiro. Ao invés
de exorcizar o sintoma e recolocar o homem na boa ordem da teia, é
necessário, num amigamento do sintoma, percorrê"-lo e perfurá"-lo até o
fim e, atravessando todos os seus mascaramentos mundanos e todas as suas
paragens legíveis, chegar novamente ao encontro da pureza do enigma.
Reencontrar a acolhida no enigmático é desencontrar"-se da ordem do
mundo: Heidegger mostrou claramente que o rosto do homem só advém quando
o primeiro \emph{self} (si"-mesmo) é aniquilado e destruído. Nesse
sentido, a ruptura"-dor e o deslocamento"-sintoma são amigos do homem; são
seus embaixadores: lá onde está uma dor está a chance do acontecimento
humano, pela simples razão de que toda dor ainda dizível é sempre uma
tradução negociada e uma reminiscência do indizível da angústia. O~psicanalista, lá onde houver ainda psicanálise real, será sempre um
parteiro de evento e nunca um pedagogo ortopédico, um zelador da boa
ordem da teia. Se o homem"-de"-Dentro chegou ao divã com uma fobia na
pinta escura da nuca, então ele está mostrando que a ruptura começou,
que há uma rachadura em sua aparente consistência. E é um psicanalista
aquele que conversa com o homem debaixo da pinta e favorece o seu
direito de retorno. Poderá continuar a existir psicanálise enquanto
houver alguém que mantenha viva a frase de Willian Blake: ``o que tem de
ser destruído deve ser destruído'', e haverá cuidado humano lá onde se
favoreça a conversão ontológica do homem conjugado na voz impessoal para
o homem da primeira pessoa.

A medicalização, por sua vez, é apenas o modo do homem moderno
assinalar, ainda, sua relação com a angústia, relação que --- na
insistência pela medicalização --- se dá na forma da fuga e do
esquivamento e não no modo da acolhida. Quando vestimos o enigma humano
na forma do dispositivo físico"-químico, podemos, sem dúvida, controlar e
intervir nos afetos e nos humores, mas a humoridade como tal permanecerá
inabordável e o seu recado não será escutado. E~qual é o recado do
humor? O humor assinala o mistério do arremesso, indica que se estamos
na teia não pertencemos inteiramente a ela, se estamos aqui no mundo não
somos inteiramente dele, pois pertencemos também à noite: temos uma
dupla pertença e somos, simultaneamente, hóspedes e estrangeiros nessa
terra… O problema ético, portanto, não está propriamente na
medicalização, mas no pressuposto instituinte segundo o qual somos
apenas um processo natural: tal proposição não coincide com a proposição
da existência.

A medicalização encobre, também, um paradoxo inerente a ela mesma, a
saber, que o aumento do controle e do domínio é diretamente proporcional
à fobia do acidente e o progresso da medicina depende, intrinsecamente,
do progresso da doença. Qualquer um pode ``sentir'' isso cotidianamente,
pode perguntar ao titio como era ir ao Fleury e ao Lavoisier vinte anos
atrás e como é ir ao Lavoisier em 2001: o avanço do controle é igual ao
avanço da paranoia. Pode ``sentir'' isso cotidianamente ao caminhar por
um lugar de segurança máxima e, mesmo num shopping"-center repleto de
câmeras, pressente"-se, ao se caminhar por um lugar de segurança máxima,
a iminência cada vez maior da possibilidade do
Desastre\footnote{Não há, portanto, nenhuma saída dentro do pensamento metafísico e a
ideia de progresso, completamente liberada na investigação científica,
constitui, hoje, o problema filosófico por excelência… aquilo que
deve ser pensado…} . Não é uma coincidência que boa parte da
melhor literatura atual trate da paranoia. Isso não acontece porque os
escritores são loucos: o afeto do acidente, o afeto Chernobyl, não é um
afeto psicológico ou idiossincrático, mas um afeto ontológico que revela
um aspecto essencial da modernidade. O~mesmo deve ser dito do sentimento
de devastação… No outro programa que assisti na mesma tarde, vi
um bando de turistas de camisas floridas, apinhados no convés de um
transatlântico com enormes máquinas fotográficas. Eles estavam
assistindo, conforme o narrado, à dança das baleias. Alguns
biólogos"-de"-barba circulavam em barcas. Eles monitoravam as baleias e
fotografavam o número"-de"-controle na pele dos mamíferos. Assim, o velho
oceano começa a entrar para um devir"-aquário, e o assassinato de Moby
Dick é zerado na recriação ecológica da baleia"-ambiente. No bloco"-imagem
subsequente, uma reportagem sobre o Tahiti mostrava claramente (sem
querer) que tudo aquilo que havia sido o Tahiti era agora ressuscitado e
mantido artificialmente para servir de objeto do consumo turístico. Ao
final, a imagem de um casal em um pedalinho num mar azul iluminado pelo
crepúsculo estava acompanhada da seguinte voz narrativa: ``Aqui, até o
sol aparece sob medida!''

Como suspender a trituração do mundo? Se viajar é ter um corpo engajado
no Desconhecido, então, como um casal incapaz de viagem poderá saudar o
recém"-chegado a não ser com o olho gelado da objetificação? Como
conduzir o homem instalado, o homem da presença, até a bênção do colapso
e a luz do escuro? Como furar o dispositivo físico"-químico até o
mistério onto"-ontológico do corpo? Fernando Pessoa, assim como muitos
outros poetas (Kafka incluído) era chamado de Isto, de Coisa, pelos seus
familiares. Isso acontece porque a cultura do Dentro baniu o poeta para
o exílio. O~pensamento do Fora deve, antes de mais nada, começar a
inverter essa equação e dizer: Isto é a família de Fernando Pessoa,
Coisa é o pai de Kafka. O~poeta deve mostrar que quem se encontra no
exílio é o homem instalado e que, longe do umbral, longe do lugar da
manifestação, a presença das coisas endurece e o admirável da situação
humana não é sequer pressentido. Se Fernando Pessoa e Kafka não puderam,
àquela altura, superar a cultura do Dentro, o primeiro por cultivar,
ainda, a saudade do mundo e de inserção (o que revela um atrelamento ao
si mesmo) e o segundo porque não pôde ver que a impotência metafísica é
a potência poética, hoje, nós, com a ajuda de Heidegger, estamos
finalmente em condições de dar o salto: podemos mostrar ao homem
metafísico, tanto ao da fé quanto ao da teoria, que a luz do conceito e
a luz do sentido, isto é, a luz no interior da qual ele mora, é apenas a
indigência que encobre o enigma da presentificação e a doação original
do mundo. Podemos mostrar que a palavra normal e preenchida, aquela
palavra que mostra apenas o que mostra, pode dar lugar à palavra
quebrada. O~homem da palavra quebrada é o homem ético porque tornou"-se o
mortal que é. Ele diz: ``se o que eu te mostro é superior ao que te
escondo, então ainda não vale o que eu te digo''. O~escondido e o
indizível devem aflorar junto da palavra, devem finitizar e arrebentar a
palavra dentro dela mesma. A~palavra quebrada, ao nomear a presença das
coisas, aponta para além delas mesmas, aponta para o mistério da
aparição e para a dimensão do ocultamento. Abrir"-se para além da
presença do ente é dizê"-lo de uma tal maneira que o não"-ente apareça
também, e simultaneamente. Então, quando isso acontece, é possível, com
Macedonio Fernández, aprender a saudar:

\emph{Vi teu filho saudar}

\emph{E então compreendi o que era saudar.}

\emph{É reconhecer alguém com tanta insistência}

\emph{Como a de Deus quando convida uma alma a existir.}

\emph{Saudar é isso, e isso eu não sabia.}

\emph{Como recompensa de ensinamento tão valioso}

\emph{Diga a teu filho}

\emph{Que receberemos uma saudação no ocaso.
{[}…{]}\footnote{Esse poema, intitulado ``Ao Filho de um Amigo'', foi dedicado a Jorge
Borges, pai do poeta Jorge Luis Borges; o garoto em questão, portanto, é
o poeta quando criança.} }

\section{Bibliografia}

\begin{itemize}
\item
  \versal{BAUDRILLARD}, Jean. Da Sedução. Campinas, Papirus, 1992.
\item
  \_\_\_\_\_\_\_\_. Simulacros e Simulação. Lisboa, Relógio d'Água,
  1991.
\item
  \versal{BEAUFRET}, Jean. Introdução às Filosofias da Existência. São Paulo,
  Duas Cidades, 1976.
\item
  \versal{FERNÁNDEZ}, Macedonio. Tudo e Nada. Rio de Janeiro, Imago, 1998.
\item
  \versal{HEIDEGGER}, Martin. Nietzsche \versal{II}. Paris, Gallimard, 1971.
\item
  \_\_\_\_\_\_\_\_. Questions \versal{III} e \versal{IV}. Paris, Gallimard, 1976.
\item
  \_\_\_\_\_\_\_\_. Essais et Conférences. Paris, Gallimard, 1958.
\item
  \_\_\_\_\_\_\_\_. History of the concept of time. Indianapolis,
  Indiana University Press, 1985.
\item
  \_\_\_\_\_\_\_\_. A~Origem da Obra de Arte. Lisboa, Edições 70,1992.
\item
  \versal{HYPPOLITE}, Jean. Ensaios de Psicanálise e Filosofia. Rio de Janeiro,
  Taurus"-Timbre, 1989.
\item
  \versal{KHAN}, M\,Masud R\,Quando a Primavera Chegar --- Despertar em
  Psicanálise Clínica. São Paulo, Escuta, 1991.
\item
  \versal{LEVI}, Primo. É~Isso um Homem? Rio de Janeiro, Rocco, 1997.
\item
  \_\_\_\_\_\_\_\_. Os Afogados e os Sobreviventes. São Paulo, Paz
  eTerra, 1990.
\item
  \versal{LOPARIC}, Zeljko. ``A Máquina no Homem'', Psicanálise e Universidade,
  n.7,1997. pp\,97--113.
\item
  \versal{LYOTARD}, Jean"-François. Heidegger e os Judeus. Petrópolis, Vozes,
  1994.
\item
  \versal{NIETZSCHE}, F\,Obras Incompletas. Os Pensadores. São Paulo, Abril
  Cultural, 1978.
\item
  \versal{NUNES}, Benedito. Crivo de Papel. São Paulo, Ática, 1998.
\item
  \versal{PERRONE}-\versal{MOISÉS}, Leyla. Fernando Pessoa --- Aquém do Eu, Além do Outro.
  São Paulo, Martins Fontes, 1982.
\item
  \versal{PESSANHA}, Juliano Garcia. Sabedoria do Nunca. São Paulo, Ateliê
  Editorial, 1999.
\item
  \versal{PRADO} \versal{JR}., Bento. Alguns Ensaios. São Paulo, Max Limonad, 1985.
\item
  \versal{SCHERER}, Rene y Kelkel, Arion Lothar. Heidegger. Madrid, Biblioteca
  Edaf, 1981.
\item
  \versal{SEARLES}, Harold F\,My Work with Bordeline Patientes. Northvale, New
  Jersey, London. Jason Aronson Inc., 1986.
\item
  \_\_\_\_\_\_\_\_. Veffort four rendre l'autrefou. Paris, Gallimard,
  1977.
\item
  \versal{SECHEHAYE}, M\,A\,La Relación Simbólica y Diario de una Esquizofrênica.
  México, Fondo de Cultura Económica, 1958.
\item
  \versal{STEINER}, George. Heidegger. London, Fontana Press, 1978.
\item
  \versal{WINNICOTT}, D\,W\,O~Brincar \& a Realidade. Rio de Janeiro, Imago,
  1975.
\end{itemize}
